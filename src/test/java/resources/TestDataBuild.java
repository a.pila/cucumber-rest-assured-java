package resources;

import io.restassured.RestAssured;
import pojo.Location;
import pojo.PostLocation;

import java.util.ArrayList;
import java.util.List;

public class TestDataBuild {

    public PostLocation AddPlacePayload(String name, String language, String address) {

        RestAssured.baseURI = "https://rahulshettyacademy.com";

        PostLocation postLocation = new PostLocation();
        postLocation.setAccuracy(50);
        postLocation.setAddress(address);
        postLocation.setLanguage(language);
        postLocation.setPhone_number("  983 893 3937");
        postLocation.setWebsite("http://google.com");
        postLocation.setName(name);

        List<String> myList = new ArrayList<String>();
        myList.add("first element");
        myList.add("second element");
        postLocation.setTypes(myList);

        Location locationObj = new Location();
        locationObj.setLat(-38.383494);
        locationObj.setLng(-40.383440);
        postLocation.setLocation(locationObj);
        return postLocation;
    }

    public String deletePlacePayload(String placeId) {
        return "{\n" +
                "    \"place_id\": \""+placeId+"\"\n" +
                "}";
    }
}
