package stepDefinition;

import io.cucumber.java.Before;

import java.io.IOException;

public class Hooks {

    @Before("@DeletePlace")
    public void beforeScenario() throws IOException {

        StepDefinition step = new StepDefinition();

        if (StepDefinition.place_id == null) {
            step.add_place_payload_with("Adele", "English", "Address of Adele");
            step.user_calls_with_http_request("postLocationAPI", "POST");
            step.verify_place_id_created_maps_to_using("Adele", "getPlaceAPI");
        }
    }
}
