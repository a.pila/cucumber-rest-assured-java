# Test suite using Rest Assured & Java
This repository contains the concepts illustrated in this Udemy course:
https://www.udemy.com/course/rest-api-automation-testing-rest-assured/

### Tools
- Rest Assured (for BDD testing)
- Java
- Maven

### Compiles and runs all the tests 
`mvn test`

### Run the tests under a specific tag
```mvn test -Dcucumber.options="--tags @Regression"```

### Clean the *target* folder before the execution
`mvn clean test`

### Generate Cucumber reports
```mvn test verify```

### Postman collection
It contains the requests that are used in this suite & can be found at this path:
`src/postmanCollection`



